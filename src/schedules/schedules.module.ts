import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { SchedulesController } from './controllers/schedules.controller';
import { ScheduleFactory } from './providers/schedule.factory';
import { ScheduleRepository } from './providers/schedule.repository';
import { SchedulesService } from './providers/schedules.service';
import { ScheduleEntity, ScheduleSchema } from './providers/schedule.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ScheduleEntity.name, schema: ScheduleSchema },
    ]),
  ],
  providers: [SchedulesService, ScheduleRepository, ScheduleFactory],
  controllers: [SchedulesController],
})
export class SchedulesModule {}
