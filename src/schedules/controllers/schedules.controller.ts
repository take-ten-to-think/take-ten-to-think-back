import {Body, Controller, Delete, Get, Param, Post, Req, UseGuards,} from '@nestjs/common';
import {Observable} from 'rxjs';

import {JwtAuthGuard} from '../../auth/jwt.auth.guard';
import {AddPresentationRequest, CreateScheduleRequest, SchedulesService,} from '../providers/schedules.service';
import { Request } from 'express';
import { UserEntity } from 'src/users/providers/user.schema';


class AddPresentationControllerRequest {
  date: string;
  topicId: string;
}

@Controller('schedules')
export class SchedulesController {
  constructor(private readonly schedulesService: SchedulesService) {}

  @Get()
  findAll(): Observable<any[]> {
    return this.schedulesService.findAll();
  }

  @Get(':id')
  find(@Param('id') id: string): Observable<any> {
    return this.schedulesService.find(id);
  }

  @Get(':id/available-dates')
  getAvailableDates(@Param('id') id: string): Observable<any> {
    return this.schedulesService.getAvailableDates(id);
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  create(
      @Body() createScheduleRequest: CreateScheduleRequest,
      ): Observable<any> {
    return this.schedulesService.create(createScheduleRequest);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  delete(@Param('id') id: string): Observable<any> {
    return this.schedulesService.delete(id);
  }

  @Post(':id/presentations')
  @UseGuards(JwtAuthGuard)
  addPresentation(
    @Param('id') id: string,
    @Body() addPresentationRequest: AddPresentationControllerRequest,
    @Req() request: Request): Observable<any> {
    const user = request.user as UserEntity;
    return this.schedulesService.addPresentation({
      scheduleId: id,
      presenterId: user.userId, 
      date: new Date(addPresentationRequest.date),
      topicId: addPresentationRequest.topicId
    } as AddPresentationRequest);
  }
  
}
