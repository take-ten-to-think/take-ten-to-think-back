import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ScheduleFactory } from '../providers/schedule.factory';
import { ScheduleRepository } from '../providers/schedule.repository';
import { SchedulesService } from '../providers/schedules.service';
import { ScheduleEntity } from '../providers/schedule.schema';
import { SchedulesController } from './schedules.controller';

describe('Schedules Controller', () => {
  let controller: SchedulesController;

  const scheduleModel = {
    find: () => {
      return {
        exec: () => Promise.resolve([{}]),
      };
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SchedulesController],
      providers: [
        ScheduleRepository,
        ScheduleFactory,
        SchedulesService,
        {
          provide: getModelToken(ScheduleEntity.name),
          useValue: scheduleModel,
        },
      ],
    }).compile();

    controller = module.get<SchedulesController>(SchedulesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
