import { Presentation } from './value.objects/presentation';

const MillisecondsPerHour: number = 1000 * 60 * 60;
const MillisecondsPerDay: number = MillisecondsPerHour * 24;

export interface ScheduleRule {
  firstDate: Date;
  daysBetweenOccurrences: number;
}

export class NameCannotBeFalsyException {}
export class ScheduleRuleMustBeValidException {}
export class UntilDateMustBeGreaterThanRuleFirstDateException {}
export class DateAlreadyScheduledException {}
export class DateDoesNotMatchScheduleRuleException {}
export class TopicIdCannotBeNullException {}

export class Schedule {
  public readonly id: string;
  public name: string;
  public image: string;
  public scheduleRule: ScheduleRule;
  public presentations: Array<Presentation> = [];

  constructor(private readonly _id: string) {
    this.id = _id;
  }

  public getAvailableDates(from: Date, until: Date): Array<Date> {
    this.validateScheduleRule(this.scheduleRule);

    if (until.getTime() - this.scheduleRule.firstDate.getTime() < 0)
      throw new UntilDateMustBeGreaterThanRuleFirstDateException();

    const diffBetweenNowAndFirstDate =
      from.getTime() - this.scheduleRule.firstDate.getTime();
    let nextDate = null;

    if (diffBetweenNowAndFirstDate > 0) {
      const daysToNextDate =
        this.scheduleRule.daysBetweenOccurrences -
        ((diffBetweenNowAndFirstDate / MillisecondsPerDay) %
          this.scheduleRule.daysBetweenOccurrences);

      const tempDate = new Date(
        from.getTime() + MillisecondsPerDay * daysToNextDate,
      );

      nextDate = new Date(this.scheduleRule.firstDate);
      nextDate.setMonth(tempDate.getMonth());
      nextDate.setFullYear(tempDate.getFullYear());
      nextDate.setDate(tempDate.getDate());
    } else {
      nextDate = new Date(this.scheduleRule.firstDate);
    }

    const availableDates = new Array<Date>();
    const scheduledDates = this.getScheduledDates();

    while (until.getTime() - nextDate.getTime() > 0) {
      if (
        nextDate.getTime() - from.getTime() > MillisecondsPerHour &&
        !scheduledDates.find(
          scheduleDate => scheduleDate.getTime() == nextDate.getTime(),
        )
      ) {
        availableDates.push(nextDate);
      }
      nextDate = new Date(
        nextDate.getTime() +
          MillisecondsPerDay * this.scheduleRule.daysBetweenOccurrences,
      );
    }

    return availableDates;
  }

  public setScheduleRule(scheduleRule: ScheduleRule): void {
    this.validateScheduleRule(scheduleRule);

    scheduleRule.firstDate.setSeconds(0);
    scheduleRule.firstDate.setMilliseconds(0);

    this.scheduleRule = scheduleRule;
  }

  public setName(name: string): void {
    if (!name) throw new NameCannotBeFalsyException();
    this.name = name;
  }

  public setImage(image: string): void {
    this.image = image;
  }

  public addPresentation(presentation: Presentation): void {
    if (!presentation.topicId) {
      throw new TopicIdCannotBeNullException();
    }
    this.ensureDateIsAvailableAndMatchesSchedulePolicy(presentation.date);
    this.presentations.push(presentation);
  }

  public cancelPresentation(presentation: Presentation): void {
    this.presentations = this.presentations.filter(
      p => !p.equals(presentation),
    );
  }

  private ensureDateIsAvailableAndMatchesSchedulePolicy(date: Date): void {
    const scheduledDates = this.getScheduledDates();
    if (
      scheduledDates.find(
        existingDate => existingDate.getTime() == date.getTime(),
      )
    )
      throw new DateAlreadyScheduledException();

    const diff = date.getTime() - this.scheduleRule.firstDate.getTime();
    const days = diff / MillisecondsPerDay;
    const daysFromStep = days % this.scheduleRule.daysBetweenOccurrences;

    if (daysFromStep !== 0) throw new DateDoesNotMatchScheduleRuleException();
  }

  private getScheduledDates() {
    return this.presentations.map(presentation => presentation.date);
  }

  private validateScheduleRule(scheduleRule: ScheduleRule) {
    if (!scheduleRule && scheduleRule.daysBetweenOccurrences > 0)
      throw new ScheduleRuleMustBeValidException();
  }
}
