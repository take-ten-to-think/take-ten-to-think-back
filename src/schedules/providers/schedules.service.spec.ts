import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ScheduleEntity } from './schedule.schema';
import { ScheduleFactory } from './schedule.factory';
import { ScheduleRepository } from './schedule.repository';
import { SchedulesService } from './schedules.service';

describe('SchedulesService', () => {
  let service: SchedulesService;

  const scheduleModel = {
    find: () => {
      return {
        exec: () => Promise.resolve([{}]),
      };
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ScheduleRepository,
        ScheduleFactory,
        SchedulesService,
        {
          provide: getModelToken(ScheduleEntity.name),
          useValue: scheduleModel,
        },
      ],
    }).compile();

    service = module.get<SchedulesService>(SchedulesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
