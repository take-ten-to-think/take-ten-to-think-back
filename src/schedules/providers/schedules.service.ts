import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {
  Schedule,
  UntilDateMustBeGreaterThanRuleFirstDateException,
} from '../schedule';
import { Presentation } from '../value.objects/presentation';
import { ScheduleFactory } from './schedule.factory';
import { ScheduleRepository } from './schedule.repository';

export interface ScheduleResponse {
  id: string;
  name: string;
  image: string;
  scheduleRule: { firstDate: Date; daysBetweenOccurrences: number };
  presentations: Array<{ presenterId: string; date: Date; topicId: string }>;
}

export interface CreateScheduleRequest {
  scheduleRule: { firstDate: Date; daysBetweenOccurrences: number };
  name: string;
  image: string;
}

export interface AddPresentationRequest {
  scheduleId: string;
  presenterId: string;
  topicId: string;
  date: Date;
}

@Injectable()
export class SchedulesService {
  constructor(
    private readonly scheduleRepository: ScheduleRepository,
    private readonly scheduleFactory: ScheduleFactory,
  ) {}

  public findAll(): Observable<ScheduleResponse[]> {
    return this.scheduleRepository
      .findAll()
      .pipe(map(schedules => schedules.map(this.mapToScheduleResponse)));
  }

  public find(id: string): Observable<ScheduleResponse> {
    return this.scheduleRepository
      .findById(id)
      .pipe(map(this.mapToScheduleResponse));
  }

  public create(
    createScheduleRequest: CreateScheduleRequest,
  ): Observable<ScheduleResponse> {
    const {
      firstDate,
      daysBetweenOccurrences,
    } = createScheduleRequest.scheduleRule;

    const schedule = this.scheduleFactory.create();

    schedule.setName(createScheduleRequest.name);
    schedule.setImage(createScheduleRequest.image);

    schedule.setScheduleRule({
      firstDate: new Date(firstDate),
      daysBetweenOccurrences,
    });

    return this.scheduleRepository.add(schedule).pipe(
      mergeMap(() => {
        return this.find(schedule.id);
      }),
    );
  }

  public delete(id: string): Observable<any> {
    return this.scheduleRepository.findById(id).pipe(
      mergeMap(schedule => {
        return this.scheduleRepository.delete(schedule.id);
      }),
    );
  }

  public getAvailableDates(id: string): Observable<any> {
    const now = new Date();
    const inAMonth = new Date();
    inAMonth.setMonth(now.getMonth() + 1);

    return this.scheduleRepository.findById(id).pipe(
      map(schedule => schedule.getAvailableDates(now, inAMonth)),
      catchError(error => {
        if (error instanceof UntilDateMustBeGreaterThanRuleFirstDateException)
          return [];
        throw error;
      }),
    );
  }

  public addPresentation(
    addPresentationRequest: AddPresentationRequest,
  ): Observable<any> {
    return this.scheduleRepository
      .findById(addPresentationRequest.scheduleId)
      .pipe(
        mergeMap(schedule => {
          const presentation = new Presentation();
          presentation.date = addPresentationRequest.date;
          presentation.topicId = addPresentationRequest.topicId;
          presentation.presenterId = addPresentationRequest.presenterId;

          schedule.addPresentation(presentation);

          return this.scheduleRepository.update(schedule).pipe(
            mergeMap(() => {
              return this.find(schedule.id);
            }),
          );
        }),
        catchError(error => {
          throw error;
        }),
      );
  }

  private mapToScheduleResponse(schedule: Schedule): ScheduleResponse {
    return {
      name: schedule.name,
      image: schedule.image,
      id: schedule.id,
      scheduleRule: schedule.scheduleRule,
      presentations: schedule.presentations.map(presentation => {
        const { presenterId, topicId, date } = presentation;
        return { presenterId, topicId, date };
      }),
    } as ScheduleResponse;
  }
}
