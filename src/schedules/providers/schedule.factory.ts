import { v4 as uuidv4 } from 'uuid';
import { Injectable } from '@nestjs/common';
import { Schedule } from '../schedule';

@Injectable()
export class ScheduleFactory {
  create(): Schedule {
    return new Schedule(`${uuidv4()}`);
  }
}
