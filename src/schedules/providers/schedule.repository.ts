import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Presentation } from '../value.objects/presentation';
import { Schedule } from '../schedule';
import { PresentationEntity, ScheduleEntity } from './schedule.schema';

@Injectable()
export class ScheduleRepository {
  constructor(
    @InjectModel(ScheduleEntity.name)
    private readonly scheduleModel: Model<ScheduleEntity>,
  ) {}

  public findById(id: string): Observable<Schedule> {
    return from(this.scheduleModel.findOne({ id: id }).exec()).pipe(
      map(this.mapScheduleEntityToSchedule),
    );
  }

  public findAll(): Observable<Array<Schedule>> {
    return from(this.scheduleModel.find().exec()).pipe(
      map(schedules => schedules.map(this.mapScheduleEntityToSchedule)),
    );
  }

  public add(schedule: Schedule): Observable<any> {
    const createdSchedule = new this.scheduleModel(
      this.mapScheduleToScheduleEntity(schedule),
    );
    return from(createdSchedule.save());
  }

  public delete(id: string): Observable<any> {
    return from(this.scheduleModel.deleteOne({ id: id }).exec());
  }

  public update(schedule: Schedule): Observable<any> {
    return from(
      this.scheduleModel
        .updateOne(
          { id: schedule.id },
          this.mapScheduleToScheduleEntity(schedule),
        )
        .exec(),
    );
  }

  private mapScheduleToScheduleEntity(schedule: Schedule): ScheduleEntity {
    const { id, name, image, scheduleRule } = schedule;
    const presentations = schedule.presentations.map(presentation => {
      const { presenterId, topicId, date } = presentation;
      return { presenterId, topicId, date } as PresentationEntity;
    });
    return { id, name, image, scheduleRule, presentations } as ScheduleEntity;
  }

  private mapScheduleEntityToSchedule(
    scheduleEntity: ScheduleEntity,
  ): Schedule {
    const schedule = new Schedule(scheduleEntity.id);

    schedule.setName(scheduleEntity.name);
    schedule.setImage(scheduleEntity.image);
    schedule.setScheduleRule(scheduleEntity.scheduleRule);

    scheduleEntity.presentations
      .map(presentationEntity => {
        const presentation = new Presentation();

        presentation.presenterId = presentationEntity.presenterId;
        presentation.topicId = presentationEntity.topicId;
        presentation.date = presentationEntity.date;

        return presentation;
      })
      .forEach(presentation => schedule.addPresentation(presentation));

    return schedule;
  }
}
