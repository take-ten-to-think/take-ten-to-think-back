import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export class PresentationEntity {
  public presenterId: string;
  public topicId: string;
  public date: Date;
}

@Schema()
export class ScheduleEntity extends Document {
  @Prop({
    unique: true,
  })
  id: string;

  @Prop() name: string;

  @Prop() image: string;

  @Prop(
    raw({
      firstDate: { type: Date },
      daysBetweenOccurrences: { type: Number },
    }),
  )
  scheduleRule;

  @Prop([PresentationEntity])
  presentations;
}

export const ScheduleSchema = SchemaFactory.createForClass(ScheduleEntity);
