export class Presentation {
    public presenterId: string;
    public topicId: string;
    public date: Date;

    public equals(other: Presentation): boolean {
        return other.date === this.date && other.presenterId === this.presenterId && other.topicId === this.topicId;
    }
}