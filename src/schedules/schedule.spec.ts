import { Presentation } from './value.objects/presentation';
import {
  DateAlreadyScheduledException,
  DateDoesNotMatchScheduleRuleException,
  Schedule,
  ScheduleRule,
} from './schedule';

describe('schedule', () => {
  let schedule: Schedule;
  let scheduleRule: ScheduleRule;

  beforeEach(() => {
    schedule = new Schedule('');
    scheduleRule = {
      firstDate: new Date('2020-07-15T14:15:00Z'),
      daysBetweenOccurrences: 7,
    };

    schedule.setScheduleRule(scheduleRule);
  });

  describe('upon construction', () => {
    it('should be defined', () => {
      expect(schedule).toBeTruthy();
    });
  });

  describe('getAvailableDates', () => {
    describe('when from is before firstDate', () => {
      it('should start from firstdate', () => {
        const from = new Date('2019-07-15T14:15:00Z');
        const until = new Date('2020-08-04T00:00:00Z');

        expect(schedule.getAvailableDates(from, until)).toEqual([
          new Date('2020-07-15T14:15:00Z'),
          new Date('2020-07-22T14:15:00Z'),
          new Date('2020-07-29T14:15:00Z'),
        ]);
      });
    });

    describe('when from is less than an hour away from firstDate', () => {
      it('should jump to next available date', () => {
        const from = new Date('2020-07-15T14:15:00Z');
        const until = new Date('2020-08-04T00:00:00Z');

        const result = schedule.getAvailableDates(from, until);

        expect(result).toEqual([
          new Date('2020-07-22T14:15:00Z'),
          new Date('2020-07-29T14:15:00Z'),
        ]);
      });
    });

    describe('when from is less than an hour away from nextDate', () => {
      it('should jump to next available date', () => {
        const from = new Date('2020-07-29T14:00:00Z');
        const until = new Date('2020-08-04T00:00:00Z');

        const result = schedule.getAvailableDates(from, until);

        expect(result).toEqual([]);
      });
    });

    describe('when a date is scheduled in the time range, it should remove it', () => {
      it('should jump to next available date', () => {
        const from = new Date('2020-09-07T00:00:00Z');
        const until = new Date('2020-10-04T00:00:00Z');

        const presentation = new Presentation();
        presentation.topicId = 'topicId';
        presentation.date = new Date('2020-09-16T14:15:00Z');
        schedule.presentations.push(presentation);

        const result = schedule.getAvailableDates(from, until);

        expect(result).toEqual([
          new Date('2020-09-09T14:15:00Z'),
          new Date('2020-09-23T14:15:00Z'),
          new Date('2020-09-30T14:15:00Z'),
        ]);
      });
    });

    describe('cancelPresentation', () => {
      it('should remve schedulded date', () => {
        const presentation = new Presentation();
        presentation.topicId = 'topicId';
        presentation.date = new Date('2020-09-16T14:15:00Z');
        schedule.presentations.push(presentation);

        schedule.cancelPresentation(presentation);

        expect(schedule.presentations).toEqual([]);
      });
    });

    describe('AddPresentation', () => {
      describe('when date in the future', () => {
        it('should schedule date', () => {
          const presentation = new Presentation();
          presentation.topicId = 'topicId';
          presentation.date = new Date('2020-09-23T14:15:00Z');

          schedule.addPresentation(presentation);

          expect(schedule.presentations).toEqual([presentation]);
        });
      });

      describe('when date is already scheduled', () => {
        it('should throw exception', () => {
          const date = new Date('2100-09-22T14:15:00Z');

          const presentation1 = new Presentation();
          presentation1.topicId = 'topicId-presentation1';
          const presentation2 = new Presentation();
          presentation2.topicId = 'topicId-presentation2';

          presentation1.date = date;
          presentation2.date = date;

          schedule.presentations.push(presentation1);

          expect(() => schedule.addPresentation(presentation2)).toThrowError(
            DateAlreadyScheduledException,
          );
        });
      });

      describe('when date at wrong schedule', () => {
        it('should throw exception', () => {
          const presentation = new Presentation();
          presentation.topicId = 'topicId';
          presentation.date = new Date('2100-09-21T14:15:00Z');

          expect(() => schedule.addPresentation(presentation)).toThrowError(
            DateDoesNotMatchScheduleRuleException,
          );
        });
      });
    });
  });
});
