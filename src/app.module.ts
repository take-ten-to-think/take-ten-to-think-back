import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';

import {AuthModule} from './auth/auth.module';
import {AppController} from './controllers/app.controller';
import {TopicsModule} from './topics/topics.module';
import {UsersModule} from './users/users.module';
import { SchedulesModule } from './schedules/schedules.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    TopicsModule,
    MongooseModule.forRoot('mongodb://localhost/take-ten-to-think'),
    SchedulesModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {
}
