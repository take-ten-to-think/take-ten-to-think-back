import {AuthService} from '../auth/providers/auth.service';

import {AppController} from './app.controller';

describe('AppController', () => {
  let appController: AppController;
  let authService: AuthService;

  beforeEach(async () => {
    authService = new AuthService(null, null);
    appController = new AppController(authService);
  });

  describe('getProfile', () => {
    it('should return user profile', async () => {
      const expectedUser = {username: 'some username', userId: 'some userId'};
      const givenRequest = {user: expectedUser};
      expect(await appController.getProfile(givenRequest)).toBe(expectedUser);
    });
  });

  describe('login', () => {
    it('should return token', async () => {
      const expected = {accessToken: 'expectedToken'};
      const givenRequest = {
        user: {username: 'some username', userId: 'some userId'}
      };
      jest.spyOn(authService, 'login')
          .mockImplementation(() => Promise.resolve(expected));
      expect(await appController.login(givenRequest)).toBe(expected);
    });
  });
});
