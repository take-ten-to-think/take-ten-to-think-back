import {Controller, Get, Post, Request, UseGuards} from '@nestjs/common';

import {JwtAuthGuard} from '../auth/jwt.auth.guard';
import {LocalAuthGuard} from '../auth/local.auth.guard';
import {AuthService} from '../auth/providers/auth.service';

@Controller()
export class AppController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() request) {
    return await this.authService.login(request.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  async getProfile(@Request() request) {
    return request.user;
  }
}
