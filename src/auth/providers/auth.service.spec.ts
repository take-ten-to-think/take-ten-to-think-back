import { JwtModule, JwtService } from '@nestjs/jwt';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';

import { UserEntity } from '../../users/providers/user.schema';
import { UsersService } from '../../users/providers/users.service';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  const userModel = {
    find: () => {
      return {
        exec: () => Promise.resolve([{}]),
      };
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: 'secret',
          signOptions: { expiresIn: '8h' },
        }),
      ],
      controllers: [],
      providers: [
        AuthService,
        UsersService,
        { provide: getModelToken(UserEntity.name), useValue: userModel },
      ],
    }).compile();
    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
