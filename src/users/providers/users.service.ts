import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

import { UserEntity } from './user.schema';

@Injectable()
export class UsersService {
  private populateUsers(): void {
    [
      {
        userId: `${uuidv4()}`,
        username: 'john',
        password: 'changeme',
      },
      {
        userId: `${uuidv4()}`,
        username: 'chris',
        password: 'secret',
      },
      {
        userId: `${uuidv4()}`,
        username: 'maria',
        password: 'guess',
      },
      {
        userId: `${uuidv4()}`,
        username: 'guest',
        password: 'guest',
      },
    ].forEach(user => {
      const createdUser = new this.userModel(user);
      createdUser
        .save()
        .then(() => {
          console.warn(`Created user: ${user.username}`);
        })
        .catch(error => {
          console.warn(`not created user: ${user.username}`);
          console.warn(error);
        });
    });
  }

  constructor(
    @InjectModel(UserEntity.name) private userModel: Model<UserEntity>,
  ) {
    this.userModel
      .find()
      .exec()
      .then(users => {
        if (!users.length) {
          this.populateUsers();
        }
      });
  }

  async findOne(username: string): Promise<UserEntity> {
    return await this.userModel.findOne({ username: username }).exec();
  }
}
