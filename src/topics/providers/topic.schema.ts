import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class TopicEntity extends Document {
  @Prop({
    unique: true,
  })
  id: string;

  @Prop() creatorId: string;

  @Prop() name: string;

  @Prop() description: string;

  @Prop() image: string;

  @Prop([String]) voters: Array<string>;
}

export const TopicSchema = SchemaFactory.createForClass(TopicEntity);
