import {Injectable} from '@nestjs/common';
import {v4 as uuidv4} from 'uuid';

import {Topic} from '../topic';

@Injectable()
export class TopicFactory {
  public create(creatorId: string): Topic {
    return new Topic(`${uuidv4()}`, creatorId);
  }
}
