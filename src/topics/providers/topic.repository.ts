import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, observable, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Topic } from '../topic';

import { TopicEntity } from './topic.schema';

@Injectable()
export class TopicRepository {
  constructor(
    @InjectModel(TopicEntity.name)
    private readonly topicModel: Model<TopicEntity>,
  ) {}

  public findById(id: string): Observable<Topic> {
    return from(this.topicModel.findOne({ id: id }).exec()).pipe(
      map(this.mapTopicEntityToTopic),
    );
  }

  public findAll(): Observable<Array<Topic>> {
    return from(this.topicModel.find().exec()).pipe(
      map(topics => topics.map(this.mapTopicEntityToTopic)),
    );
  }

  public delete(id: string): Observable<any> {
    return from(this.topicModel.deleteOne({ id: id }).exec());
  }

  public update(topic: Topic): Observable<any> {
    return from(
      this.topicModel
        .updateOne({ id: topic.id }, this.mapTopicToTopicEntity(topic))
        .exec(),
    );
  }

  public add(topic: Topic): Observable<any> {
    const createdTopic = new this.topicModel(this.mapTopicToTopicEntity(topic));
    return from(createdTopic.save());
  }

  private mapTopicEntityToTopic(topicEntity: TopicEntity): Topic {
    if (!topicEntity) return null;

    const topic = new Topic(topicEntity.id, topicEntity.creatorId);

    topic.name = topicEntity.name;
    topic.description = topicEntity.description;
    topic.image = topicEntity.image;
    topic.voters = topicEntity.voters;

    return topic;
  }

  private mapTopicToTopicEntity(topic: Topic): TopicEntity {
    const { id, name, description, image, creatorId, voters } = topic;
    return {
      id,
      name,
      description,
      image,
      creatorId,
      voters,
    } as TopicEntity;
  }
}
