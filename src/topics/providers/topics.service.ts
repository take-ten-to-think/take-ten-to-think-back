import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { Topic } from '../topic';

import { TopicFactory } from './topic.factory';
import { TopicRepository } from './topic.repository';

export class OnlyCreatorCanDeleteTopicException {}
export class OnlyCreatorCanUpdateTopicException {}
export class TopicNotFoundException {}

export class CreateTopicRequest {
  name: string;
  description: string;
  image: string;
}

export class UpdateTopicRequest {
  name: string;
  description: string;
  image: string;
}

export interface TopicResponse {
  id: string;
  name: string;
  description: string;
  image: string;
  creatorId: string;
  voters: Array<string>;
}

@Injectable()
export class TopicsService {
  constructor(
    private readonly topicRepository: TopicRepository,
    private readonly topicFactory: TopicFactory,
  ) {}

  private mapToTopic(topic: Topic): TopicResponse {
    if (!topic) return null;
    return {
      id: topic.id,
      name: topic.name,
      description: topic.description,
      image: topic.image,
      creatorId: topic.creatorId,
      voters: topic.voters,
    } as TopicResponse;
  }

  public findAll(): Observable<Array<TopicResponse>> {
    return this.topicRepository
      .findAll()
      .pipe(map(topics => topics.map(this.mapToTopic)));
  }

  public find(id: string): Observable<TopicResponse> {
    return this.topicRepository.findById(id).pipe(map(this.mapToTopic));
  }

  public create(
    createTopicRequest: CreateTopicRequest,
    userId: string,
  ): Observable<TopicResponse> {
    const { name, description, image } = createTopicRequest;

    const topic = this.topicFactory.create(userId);

    topic.setName(name);
    topic.setDescription(description);
    topic.setImage(image);

    return this.topicRepository.add(topic).pipe(
      mergeMap(() => {
        return this.find(topic.id);
      }),
    );
  }

  public update(
    id: string,
    userId: string,
    updateTopicRequest: UpdateTopicRequest,
  ): Observable<TopicResponse> {
    return this.topicRepository.findById(id).pipe(
      mergeMap(topic => {
        if (!topic) throw new TopicNotFoundException();
        if (topic.creatorId !== userId) {
          throw new OnlyCreatorCanUpdateTopicException();
        }

        const { name, description, image } = updateTopicRequest;

        topic.setName(name);
        topic.setDescription(description);
        topic.setImage(image);

        return this.topicRepository.update(topic).pipe(
          mergeMap(() => {
            return this.find(topic.id);
          }),
        );
      }),
    );
  }

  public delete(id: string, userId: string): Observable<any> {
    return this.topicRepository.findById(id).pipe(
      mergeMap(topic => {
        if (!topic) throw new TopicNotFoundException();

        if (topic.creatorId !== userId)
          throw new OnlyCreatorCanDeleteTopicException();

        return this.topicRepository.delete(id);
      }),
    );
  }

  public vote(id: string, userId: string): Observable<TopicResponse> {
    return this.topicRepository.findById(id).pipe(
      mergeMap(topic => {
        if (!topic) throw new TopicNotFoundException();

        topic.addVoter(userId);

        return this.topicRepository.update(topic).pipe(
          mergeMap(() => {
            return this.find(topic.id);
          }),
        );
      }),
    );
  }
}
