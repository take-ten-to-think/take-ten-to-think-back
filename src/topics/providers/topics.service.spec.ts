import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';

import { TopicFactory } from './topic.factory';
import { TopicRepository } from './topic.repository';
import { TopicEntity } from './topic.schema';
import { TopicsService } from './topics.service';

describe('TopicsService', () => {
  let service: TopicsService;
  let topicModel = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TopicsService,
        TopicRepository,
        TopicFactory,
        { provide: getModelToken(TopicEntity.name), useValue: topicModel },
      ],
    }).compile();

    service = module.get<TopicsService>(TopicsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
