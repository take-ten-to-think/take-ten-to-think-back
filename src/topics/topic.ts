export class TopicNameCannotBeEmptyException {}
export class TopicDescriptionCannotBeEmptyException {}

export class Topic {
  public readonly id: string;
  public readonly creatorId: string;
  public voters: Array<string> = [];
  public name: string;
  public description: string;
  public image: string;

  constructor(
      private readonly _id: string,
      private readonly _creatorId: string,
  ) {
    this.id = _id;
    this.creatorId = _creatorId;
  }

  public setImage(image: string): void {
    this.image = image;
  }

  public setDescription(description: string): void {
    if (!description) {
      throw new TopicDescriptionCannotBeEmptyException();
    }

    this.description = description;
  }

  public setName(name: string): void {
    if (!name) {
      throw new TopicNameCannotBeEmptyException();
    }

    this.name = name;
  }

  public hasVoter(userId: string): boolean {
    return !!this.voters.find(voterId => voterId === userId);
  }

  public addVoter(userId: any): void {
    if (!this.hasVoter(userId)) {
      this.voters.push(userId);
    }
  }
}
