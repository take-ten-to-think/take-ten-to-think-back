import {Test, TestingModule} from '@nestjs/testing';

import {TopicFactory} from '../providers/topic.factory';
import {TopicRepository} from '../providers/topic.repository';
import {TopicsService} from '../providers/topics.service';

import {TopicsController} from './topics.controller';

describe('Topics Controller', () => {
  let controller: TopicsController;
  let topicRepository = new TopicRepository(null);

  beforeEach(async () => {
    const module: TestingModule = await Test
                                      .createTestingModule({
                                        controllers: [TopicsController],
                                        providers: [
                                          TopicsService,
                                          TopicFactory,
                                          {
                                            provide: TopicRepository,
                                            useValue: topicRepository,
                                          },
                                        ]
                                      })
                                      .compile();

    controller = module.get<TopicsController>(TopicsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
