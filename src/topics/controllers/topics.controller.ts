import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UserEntity } from 'src/users/providers/user.schema';

import { JwtAuthGuard } from '../../auth/jwt.auth.guard';
import {
  CreateTopicRequest,
  OnlyCreatorCanDeleteTopicException,
  OnlyCreatorCanUpdateTopicException,
  TopicNotFoundException,
  TopicsService,
  UpdateTopicRequest,
} from '../providers/topics.service';

@Controller('topics')
export class TopicsController {
  constructor(private readonly topicsService: TopicsService) {}

  @Get()
  findAll(): Observable<any[]> {
    return this.topicsService.findAll();
  }

  @Get(':id')
  find(@Param('id') id: string): Observable<any> {
    return this.topicsService.find(id).pipe(
      map(topic => {
        if (!topic) throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        else return topic;
      }),
    );
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  create(
    @Body() createTopicRequest: CreateTopicRequest,
    @Req() request: Request,
  ): Observable<any> {
    const user = request.user as UserEntity;
    return this.topicsService.create(createTopicRequest, user.userId);
  }

  @Put(':id')
  @UseGuards(JwtAuthGuard)
  update(
    @Param('id') id: string,
    @Body() updateTopicRequest: UpdateTopicRequest,
    @Req() request: Request,
  ): Observable<any> {
    const user = request.user as UserEntity;
    return this.topicsService.update(id, user.userId, updateTopicRequest).pipe(
      catchError(error => {
        if (error instanceof TopicNotFoundException) {
          throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        } else if (error instanceof OnlyCreatorCanUpdateTopicException) {
          throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
        } else throw error;
      }),
    );
  }

  @Put(':id/vote')
  @UseGuards(JwtAuthGuard)
  vote(@Param('id') id: string, @Req() request: Request): Observable<any> {
    const user = request.user as UserEntity;
    return this.topicsService.vote(id, user.userId).pipe(
      catchError(error => {
        if (error instanceof TopicNotFoundException) {
          throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        } else throw error;
      }),
    );
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  delete(@Param('id') id: string, @Req() request: Request): Observable<any> {
    const user = request.user as UserEntity;
    return this.topicsService.delete(id, user.userId).pipe(
      catchError(error => {
        if (error instanceof TopicNotFoundException) {
          throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        } else if (error instanceof OnlyCreatorCanDeleteTopicException) {
          throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
        } else throw error;
      }),
    );
  }
}
