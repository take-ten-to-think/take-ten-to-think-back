import {TopicFactory} from './providers/topic.factory';
import {Topic, TopicDescriptionCannotBeEmptyException, TopicNameCannotBeEmptyException} from './topic';

describe('topic', () => {
  let topicFactory: TopicFactory = new TopicFactory();
  let topic: Topic;

  beforeEach(() => {
    topic = topicFactory.create('');
  });

  it('should be defined', () => {
    expect(topic).toBeDefined();
  });

  describe('setName when name is falsy', () => {
    it('should throw exception', () => {
      expect(() => topic.setName(undefined))
          .toThrowError(TopicNameCannotBeEmptyException);
    });
  });

  describe('set image', () => {
    it('should change image', () => {
      const expectedImage = 'some image link';
      topic.setImage(expectedImage);
      expect(topic.image).toBe(expectedImage);
    });
  });

  describe('set description when description is falsy', () => {
    it('should throw exception', () => {
      expect(() => topic.setDescription(undefined))
          .toThrowError(TopicDescriptionCannotBeEmptyException);
    });
  })

  describe('set description when description is valid', () => {
    it('should change description', () => {
      const expectedDescription = 'some description';
      topic.setDescription(expectedDescription);
      expect(topic.description).toEqual(expectedDescription);
    });
  });

  describe('setName when name is valid', () => {
    it('should not throw exception', () => {
      const expectedName = 'expectedName';
      topic.setName(expectedName);
      expect(topic.name).toBe(expectedName);
    });
  });

  describe('add voter when voter not in voters', () => {
    it('should add voter', () => {
      const voterId = 'voterId';
      topic.addVoter(voterId);
      expect(topic.voters).toContain(voterId);
    });
  });

  describe('add voter when voter already present', () => {
    it('should not change voters list', () => {
      const voterId = 'voterId';
      const voters = ['voterId', 'voterId-2'];

      topic.voters = [...voters];

      topic.addVoter(voterId);

      expect(topic.voters).toEqual([...voters]);
    });
  });
});
