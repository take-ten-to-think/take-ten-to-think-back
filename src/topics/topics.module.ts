import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { TopicsController } from './controllers/topics.controller';
import { TopicFactory } from './providers/topic.factory';
import { TopicRepository } from './providers/topic.repository';
import { TopicEntity, TopicSchema } from './providers/topic.schema';
import { TopicsService } from './providers/topics.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: TopicEntity.name,
        schema: TopicSchema,
      },
    ]),
  ],
  controllers: [TopicsController],
  providers: [TopicsService, TopicRepository, TopicFactory],
  exports: [TopicsService],
})
export class TopicsModule {}
