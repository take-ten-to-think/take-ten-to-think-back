FROM node:latest AS builder

COPY . /app
WORKDIR /app

RUN npm i
RUN npm test
RUN npm run build

FROM node:latest


ENV NODE_ENV production

WORKDIR /app

COPY --from=builder /app/package*.json /app/
COPY --from=builder /app/dist/ /app/dist/

RUN npm ci

EXPOSE 3000
CMD ["node", "/app/dist/main.js"]  